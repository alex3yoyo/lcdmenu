#!/usr/bin/python

import os
import commands
import RPi.GPIO as GPIO
from string import split
from time import sleep, strftime, localtime
from xml.dom.minidom import *

import psutil
import smbus

from Adafruit_I2C import Adafruit_I2C
from Adafruit_MCP230xx import Adafruit_MCP230XX
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate
from ListSelector import ListSelector


if commands.getoutput("sudo i2cdetect -y 1").split()[48] == "--":
    print("Error: LCD plate not connected to I2C address 0x20")
    quit()

selSleepTime = 0.15
backSleepTime = 0.10

configfile = 'lcdmenu.xml'
# set DEBUG=1 for print debug statements
DEBUG = 0
DISPLAY_ROWS = 2
DISPLAY_COLS = 16

# set busnum param to the correct value for your pi (rev1 = 0, rev2 = 1)
lcd = Adafruit_CharLCDPlate(busnum = 1)

lcd.begin(DISPLAY_COLS, DISPLAY_ROWS)
lcd.backlight(lcd.OFF)

def update_message(MSG):
    """Updates LCD with MSG starting at char 1"""
    lcd.home()
    lcd.message(MSG)

def scroll_message(MSG):
    """Displays MSG on the LCD, allowing user to scroll using right button"""
    lcd.clear()
    lcd.home()
    lcd.message(MSG)
    sleep(1)
    n=1
    while 1:
        if lcd.buttonPressed(lcd.RIGHT):
            while n<len(MSG)-15:
                lcd.scrollDisplayLeft()
                n=n+1
                sleep(0.25)
        elif lcd.buttonPressed(lcd.LEFT):
            break
        sleep(backSleepTime)

def do_sleep():
    """Turns off the backlight, and back on when select is pressed"""
    lcd.clear()
    lcd.message("Press Sel to\nwake from sleep")
    sleep(2)
    lcd.clear()
    lcd.backlight(lcd.OFF)
    while 1:
        if lcd.buttonPressed(lcd.SELECT):
            lcd.backlight(lcd.ON)
            break
        sleep(backSleepTime)

def do_quit():
    """Quits lcdmenu"""
    lcd.clear()
    lcd.message('Are you sure?\nPress Sel for Y')
    while 1:
        if lcd.buttonPressed(lcd.LEFT):
            break
        if lcd.buttonPressed(lcd.SELECT):
            lcd.clear()
            lcd.backlight(lcd.OFF)
            GPIO.cleanup()
            quit()
        sleep(0.1)

def do_shutdown():
    """Shutdown Raspberry Pi"""
    lcd.clear()
    lcd.message('Are you sure?\nPress Sel for Y')
    while 1:
        if lcd.buttonPressed(lcd.LEFT):
            break
        if lcd.buttonPressed(lcd.SELECT):
            lcd.clear()
            lcd.backlight(lcd.OFF)
            commands.getoutput("sudo shutdown -h -P now Goodbye")
            quit()
        sleep(0.1)

# LCD Colors
def LCD_off():
    lcd.backlight(lcd.OFF)

def LCD_on():
    lcd.backlight(lcd.ON)

def LCD_red():
    lcd.backlight(lcd.RED)

def LCD_green():
    lcd.backlight(lcd.GREEN)

def LCD_blue():
    lcd.backlight(lcd.BLUE)

def LCD_yellow():
    lcd.backlight(lcd.YELLOW)

def LCD_teal():
    lcd.backlight(lcd.TEAL)

def LCD_violet():
    lcd.backlight(lcd.VIOLET)

# Date and Time
def date_time():
    """Display date and time, updated every backSleepTime"""
    lcd.clear()
    while 1:
        TIME_DATE = strftime('%a %b %d %Y\n%I:%M:%S %p', localtime())
        MESSAGE = str(TIME_DATE)
        update_message(MESSAGE)
        if lcd.buttonPressed(lcd.LEFT):
            break
        sleep(backSleepTime)

# Network
def sys_net_IP_address():
    lcd.clear()
    NET_IP_EN0 = commands.getoutput("/sbin/ifconfig").split("\n")[1].split()[1][5:]
    # NET_IP_WLAN0 = commands.getoutput("/sbin/ifconfig").split("\n")[1].split()[1][5:]
    MESSAGE = "en0: " + str(NET_IP_EN0)
    update_message(MESSAGE)
    while 1:
        if lcd.buttonPressed(lcd.LEFT):
            break
        sleep(backSleepTime)

# def sys_net_hostname():
    # lcd.clear()
    # NET_HOSTNAME = "hostname"
    # MESSAGE = str(NET_HOSTNAME)
    # update_message(MESSAGE)
    # if lcd.buttonPressed(lcd.LEFT):
    #     break
    # sleep(backSleepTime)

def sys_net_stat_bytes():
    lcd.clear()
    while 1:
        NET_BYTES_SENT = psutil.net_io_counters(pernic=False)[0]
        NET_BYTES_RECV = psutil.net_io_counters(pernic=False)[1]
        MESSAGE = "Sent:" + str(NET_BYTES_SENT) + "\nRecv:" + str(NET_BYTES_RECV)
        update_message(MESSAGE)
        if lcd.buttonPressed(lcd.LEFT):
            break
        sleep(backSleepTime)

def sys_net_stat_packets():
    lcd.clear()
    while 1:
        NET_PACKETS_SENT = psutil.net_io_counters(pernic=False)[2]
        NET_PACKETS_RECV = psutil.net_io_counters(pernic=False)[3]
        MESSAGE = "Sent:" + str(NET_PACKETS_SENT) + "\nRecv:" + str(NET_PACKETS_RECV)
        update_message(MESSAGE)
        if lcd.buttonPressed(lcd.LEFT):
            break
        sleep(backSleepTime)

def sys_net_stat_errors():
    lcd.clear()
    while 1:
        NET_ERRORS_IN = psutil.net_io_counters(pernic=False)[4]
        NET_ERRORS_OUT = psutil.net_io_counters(pernic=False)[5]
        MESSAGE = "In:" + str(NET_ERRORS_IN) + "\nOut:" + str(NET_ERRORS_OUT)
        update_message(MESSAGE)
        if lcd.buttonPressed(lcd.LEFT):
            break
        sleep(backSleepTime)

def sys_net_stat_drops():
    lcd.clear()
    while 1:
        NET_DROPS_IN = psutil.net_io_counters(pernic=False)[0]
        NET_DROPS_OUT = psutil.net_io_counters(pernic=False)[1]
        MESSAGE = "In:" + str(NET_DROPS_IN) + "\nOut:" + str(NET_DROPS_OUT)
        update_message(MESSAGE)
        if lcd.buttonPressed(lcd.LEFT):
            break
        sleep(backSleepTime)

# CPU
def sys_CPU_freq():
    lcd.clear()
    while 1:
        CPU_FREQ_CURRENT = int(open("/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq").read().split()[0])/1000
        CPU_FREQ_MIN = int(open("/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq").read().split()[0])/1000
        CPU_FREQ_MAX = int(open("/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq").read().split()[0])/1000
        MESSAGE = "Current: " + str(CPU_FREQ_CURRENT) + " MHz\nMin:"+ str(CPU_FREQ_MIN) + " Max:" +str(CPU_FREQ_MAX)
        update_message(MESSAGE)
        if lcd.buttonPressed(lcd.LEFT):
            break
        sleep(backSleepTime)

def sys_CPU_load():
    lcd.clear()
    while 1:
        CPU_LOAD = psutil.cpu_percent(interval=1)
        CPU_LOAD_AVG1 = os.getloadavg()[0]
        CPU_LOAD_AVG2 = os.getloadavg()[1]
        CPU_LOAD_AVG3 = os.getloadavg()[2]
        MESSAGE = "Current: " + str(CPU_LOAD) + "\n" + str(CPU_LOAD_AVG1) + ", " + str(CPU_LOAD_AVG2) + "," + str(CPU_LOAD_AVG3)
        update_message(MESSAGE)
        if lcd.buttonPressed(lcd.LEFT):
            break
        sleep(backSleepTime)

# def sys_CPU_temp():
    # lcd.clear()
    # while 1:
        # CPU_TEMP = "IDK"
        # MESSAGE = str(CPU_TEMP)
        # update_message(MESSAGE)
        # if lcd.buttonPressed(lcd.LEFT):
            # break
        # sleep(backSleepTime)

# Memory
def sys_memory():
    """Display memory statistics"""
    lcd.clear()
    while 1:
        MEM_TOTAL = int(psutil.virtual_memory()[0])/1000
        MEM_USED_PCNT = psutil.virtual_memory()[2]
        MESSAGE = "Used: " + str(MEM_USED_PCNT) + "%\nTotal: " + str(MEM_TOTAL) + " kB"
        update_message(MESSAGE)
        if lcd.buttonPressed(lcd.LEFT):
            break
        sleep(backSleepTime)

# Disks
def sys_disk_usage():
    lcd.clear()
    while 1:
        DISK_TOTAL = int(psutil.disk_usage("/")[0])/1000000000
        DISK_USED = int(psutil.disk_usage("/")[1])/1000000000
        DISK_FREE = int(psutil.disk_usage("/")[2])/1000000000
        DISK_USED_PCNT = psutil.disk_usage("/")[3]
        MESSAGE = "Used: " + str(DISK_USED_PCNT) + "% of " + str(DISK_TOTAL) + "GB\nFree: " + str(DISK_FREE) + " GB"
        update_message(MESSAGE)
        if lcd.buttonPressed(lcd.LEFT):
            break
        sleep(backSleepTime)

def sys_disk_activity_bytes():
    lcd.clear()
    while 1:
        DISK_READ = psutil.disk_io_counters(perdisk=False)[2]
        DISK_WRITE = psutil.disk_io_counters(perdisk=False)[3]
        MESSAGE = "Read:" + str(DISK_READ) + "\nWrite:" + str(DISK_WRITE)
        update_message(MESSAGE)
        if lcd.buttonPressed(lcd.LEFT):
            break
        sleep(backSleepTime)

def sys_disk_activity_IOs():
    lcd.clear()
    while 1:
        DISK_READ = psutil.disk_io_counters(perdisk=False)[0]
        DISK_WRITE = psutil.disk_io_counters(perdisk=False)[1]
        MESSAGE = "Read:" + str(DISK_READ) + "\nWrite:" + str(DISK_WRITE)
        update_message(MESSAGE)
        if lcd.buttonPressed(lcd.LEFT):
            break
        sleep(backSleepTime)

def sys_disk_activity_times():
    lcd.clear()
    while 1:
        DISK_READ = psutil.disk_io_counters(perdisk=False)[4]
        DISK_WRITE = psutil.disk_io_counters(perdisk=False)[5]
        MESSAGE = "Read:" + str(DISK_READ) + "\nWrite:" + str(DISK_WRITE)
        update_message(MESSAGE)
        if lcd.buttonPressed(lcd.LEFT):
            break
        sleep(backSleepTime)

# The guts
class CommandToRun:
    def __init__(self, myName, theCommand):
        self.text = myName
        self.commandToRun = theCommand
    def Run(self):
        self.clist = split(commands.getoutput(self.commandToRun), '\n')
        if len(self.clist) > 0:
            lcd.clear()
            lcd.message(self.clist[0])
            for i in range(1, len(self.clist)):
                while 1:
                    if lcd.buttonPressed(lcd.DOWN):
                        break
                    sleep(0.25)
                lcd.clear()
                lcd.message(self.clist[i-1]+'\n'+self.clist[i])
                sleep(0.5)
        while 1:
            if lcd.buttonPressed(lcd.LEFT):
                break

class Widget:
    def __init__(self, myName, myFunction):
        self.text = myName
        self.function = myFunction

class Folder:
    def __init__(self, myName, myParent):
        self.text = myName
        self.items = []
        self.parent = myParent

def HandleSettings(node):
    global lcd
    if node.getAttribute('lcdColor').lower() == 'red':
        lcd.backlight(lcd.RED)
    elif node.getAttribute('lcdColor').lower() == 'green':
        lcd.backlight(lcd.GREEN)
    elif node.getAttribute('lcdColor').lower() == 'blue':
        lcd.backlight(lcd.BLUE)
    elif node.getAttribute('lcdColor').lower() == 'yellow':
        lcd.backlight(lcd.YELLOW)
    elif node.getAttribute('lcdColor').lower() == 'teal':
        lcd.backlight(lcd.TEAL)
    elif node.getAttribute('lcdColor').lower() == 'violet':
        lcd.backlight(lcd.VIOLET)
    elif node.getAttribute('lcdColor').lower() == 'white':
        lcd.backlight(lcd.ON)
    if node.getAttribute('lcdBacklight').lower() == 'on':
        lcd.backlight(lcd.ON)
    elif node.getAttribute('lcdBacklight').lower() == 'off':
        lcd.backlight(lcd.OFF)

def ProcessNode(currentNode, currentItem):
    children = currentNode.childNodes

    for child in children:
        if isinstance(child, xml.dom.minidom.Element):
            if child.tagName == 'settings':
                HandleSettings(child)
            elif child.tagName == 'folder':
                thisFolder = Folder(child.getAttribute('text'), currentItem)
                currentItem.items.append(thisFolder)
                ProcessNode(child, thisFolder)
            elif child.tagName == 'widget':
                thisWidget = Widget(child.getAttribute('text'), child.getAttribute('function'))
                currentItem.items.append(thisWidget)
            elif child.tagName == 'run':
                thisCommand = CommandToRun(child.getAttribute('text'), child.firstChild.data)
                currentItem.items.append(thisCommand)

class Display:
    def __init__(self, folder):
        self.curFolder = folder
        self.curTopItem = 0
        self.curSelectedItem = 0
    def display(self):
        if self.curTopItem > len(self.curFolder.items) - DISPLAY_ROWS:
            self.curTopItem = len(self.curFolder.items) - DISPLAY_ROWS
        if self.curTopItem < 0:
            self.curTopItem = 0
        if DEBUG:
            print('------------------')
        str = ''
        for row in range(self.curTopItem, self.curTopItem+DISPLAY_ROWS):
            if row > self.curTopItem:
                str += '\n'
            if row < len(self.curFolder.items):
                if row == self.curSelectedItem:
                    cmd = '-'+self.curFolder.items[row].text
                    if len(cmd) < 16:
                        for row in range(len(cmd), 16):
                            cmd += ' '
                    if DEBUG:
                        print('|'+cmd+'|')
                    str += cmd
                else:
                    cmd = ' '+self.curFolder.items[row].text
                    if len(cmd) < 16:
                        for row in range(len(cmd), 16):
                            cmd += ' '
                    if DEBUG:
                        print('|'+cmd+'|')
                    str += cmd
        if DEBUG:
            print('------------------')
        lcd.home()
        lcd.message(str)

    def update(self, command):
        if DEBUG:
            print('do',command)
        if command == 'u':
            self.up()
        elif command == 'd':
            self.down()
        elif command == 'r':
            self.right()
        elif command == 'l':
            self.left()
        elif command == 's':
            self.select()
    def up(self):
        if self.curSelectedItem == 0:
            return
        elif self.curSelectedItem > self.curTopItem:
            self.curSelectedItem -= 1
        else:
            self.curTopItem -= 1
            self.curSelectedItem -= 1
        sleep(selSleepTime)
    def down(self):
        if self.curSelectedItem+1 == len(self.curFolder.items):
            return
        elif self.curSelectedItem < self.curTopItem+DISPLAY_ROWS-1:
            self.curSelectedItem += 1
        else:
            self.curTopItem += 1
            self.curSelectedItem += 1
        sleep(selSleepTime)
    def left(self):
        if isinstance(self.curFolder.parent, Folder):
            # find the current in the parent
            itemno = 0
            index = 0
            for item in self.curFolder.parent.items:
                if self.curFolder == item:
                    if DEBUG:
                        print('foundit')
                    index = itemno
                else:
                    itemno += 1
            if index < len(self.curFolder.parent.items):
                self.curFolder = self.curFolder.parent
                self.curTopItem = index
                self.curSelectedItem = index
            else:
                self.curFolder = self.curFolder.parent
                self.curTopItem = 0
                self.curSelectedItem = 0
        sleep(selSleepTime)
    def right(self):
        if isinstance(self.curFolder.items[self.curSelectedItem], Folder):
            self.curFolder = self.curFolder.items[self.curSelectedItem]
            self.curTopItem = 0
            self.curSelectedItem = 0
        elif isinstance(self.curFolder.items[self.curSelectedItem], Widget):
            if DEBUG:
                print('eval', self.curFolder.items[self.curSelectedItem].function)
            eval(self.curFolder.items[self.curSelectedItem].function+'()')
        elif isinstance(self.curFolder.items[self.curSelectedItem], CommandToRun):
            self.curFolder.items[self.curSelectedItem].Run()
        sleep(selSleepTime)

    def select(self):
        if DEBUG:
            print('check widget')
        if isinstance(self.curFolder.items[self.curSelectedItem], Widget):
            if DEBUG:
                print('eval', self.curFolder.items[self.curSelectedItem].function)
            eval(self.curFolder.items[self.curSelectedItem].function+'()')
        sleep(selSleepTime)

# now start things up
uiItems = Folder('root','')

dom = parse(configfile) # parse an XML file by name

top = dom.documentElement

ProcessNode(top, uiItems)

display = Display(uiItems)
display.display()

while 1:
    if (lcd.buttonPressed(lcd.LEFT)):
        display.update('l')
        display.display()

    if (lcd.buttonPressed(lcd.UP)):
        display.update('u')
        display.display()

    if (lcd.buttonPressed(lcd.DOWN)):
        display.update('d')
        display.display()

    if (lcd.buttonPressed(lcd.RIGHT)):
        display.update('r')
        display.display()

    if (lcd.buttonPressed(lcd.SELECT)):
        display.update('s')
        display.display()
