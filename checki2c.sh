#!/bin/bash

check_i2c() {
	if [[ $(sudo i2cdetect -y 1 0x20 0x20) == *"20: 20"* ]]; then
	    echo "LCD plate connected"
	   	return 0
	else
	    echo "LCD plate is not connected or there was an error"
	    return 1
	fi
}

check_i2c
exit
