# LcdMenu #

A menu application for Adafruit's I2C LCD plate for the Raspberry Pi.

## Dependencies ##

### Adafruit-Raspberry-Pi-Python ###

Download Adafruit's Python library at [https://github.com/Adafruit/Adafruit-Raspberry-Pi-Python](https://github.com/Adafruit/Adafruit-Raspberry-Pi-Python)

### smbus ###

Needed to communicate with the LCD plate. Run:

    pip install smbus

to install.

### psutil ###

Needed to display system information. Run:

    pip install psutil

to install.

## License ##

This project is licensed under the GPL v3 license.

## Original Project ##

This project is based off of [RaspberryPiLcdMenu](https://github.com/aufder/RaspberryPiLcdMenu) by Alan Aufderheide.
