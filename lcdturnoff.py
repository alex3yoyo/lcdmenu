#!/usr/bin/python

from Adafruit_I2C import Adafruit_I2C
from Adafruit_MCP230xx import Adafruit_MCP230XX
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate

lcd = Adafruit_CharLCDPlate()

def LCD_plate_off():
	lcd.clear()
	lcd.noDisplay()
	lcd.backlight(lcd.OFF)

LCD_plate_off()
